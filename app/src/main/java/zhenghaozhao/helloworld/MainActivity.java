package zhenghaozhao.helloworld;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity implements Runnable {
    int count = 0;
    Handler timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        timer = new Handler();
        timer.postDelayed(this,1000);
    }

    public void reset(View view) {
        count = 0;
        TextView textView = (TextView)findViewById(R.id.textView2);
        textView.setText(""+count);

    }
    public void addOne(View view) {
        count++;
        TextView textView = (TextView)findViewById(R.id.textView2);
        textView.setText(""+count);

    }
    public void toCal(View view){
        Intent intent = new Intent(this, CalActivity.class);
        startActivity(intent);

    }
    public void toChal1(View view){
        Intent intent = new Intent(this, Challenge2Activity.class);
        startActivity(intent);

    }


    @Override
    public void run() {

    }
}
