package zhenghaozhao.helloworld;

import android.annotation.TargetApi;
import android.os.Build;

/**
 * Created by Zhenghao on 8/05/2017.
 */

public class SimpleTokenizer extends Tokenizer {
    String text;
    Object current;
    int pos;

    SimpleTokenizer(String text){
        this.text = text;
        pos = 0;
        next();
    }


    @Override
    boolean hasNext() {
        return current != null;
    }

    @Override
    Object current() {
        return current;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    void next() {
        char c = 0;
        int textlen = text.length();


        if (pos < textlen)
            c = text.charAt(pos);

        if (pos >= textlen) {
            current = null;
            return;
        } else if (c == '(') {
            pos++;
            current = "(";
            return;
        }else if (c == ')') {
            pos++;
            current = ")";
            return;
        }
        else if (c == '/'){
            pos++;
            current = "/";
            return;
        }
        else if (c == '*'){
            pos++;
            current = "*";
            return;
        }
        else if (c == '+'){
            pos++;
            current = "+";
            return;
        }
        else if (c == '-'){
            pos++;
            current = "-";
            return;
        }
        else if (c == ' '){
            pos++;
            next();
            return;
        }
        else if (Character.isDigit(c)) {
            String res = "" + c;
            pos++;
            while (pos < textlen && (Character.isDigit(text.charAt(pos)) || text.charAt(pos) == '.')) {
                c = text.charAt(pos);
                res += c;
                pos++;
            }
            current = Float.parseFloat(res);
            return;
        }
        else if (Character.isAlphabetic(c)){
            String res = "" + c;
            pos++;
            while (pos < textlen && (Character.isAlphabetic(text.charAt(pos)))){
                c = text.charAt(pos);
                res += c;
                pos++;
            }
            current = res;
            return;
        }
    }
}
