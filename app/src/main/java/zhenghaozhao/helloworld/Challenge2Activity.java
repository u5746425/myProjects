package zhenghaozhao.helloworld;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

public class Challenge2Activity extends AppCompatActivity implements Runnable {
    float numClicks = 0.0f;
    Handler timer = new Handler();
    float counter = 0.0f;
    float speed = 0.0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge2);
        timer.postDelayed(this,1000);
    }

    public void clickSpeed(View view) {
        numClicks++;
        speed = numClicks/counter;
        TextView view1 = (TextView)findViewById(R.id.textView5);
        view1.setText(""+speed+"     "+counter);

    }

    public void reset(View view) {
        counter = 0;
        numClicks = 0;
        speed = 0.0f;
        TextView view2 = (TextView)findViewById(R.id.textView5);
        view2.setText(""+speed+"     "+counter);

    }

    @Override
    public void run() {
        counter++;
        speed = numClicks/counter;
        TextView view2 = (TextView)findViewById(R.id.textView5);
        view2.setText(""+speed+"     "+counter);
        timer.postDelayed(this, 1000);

    }

}
