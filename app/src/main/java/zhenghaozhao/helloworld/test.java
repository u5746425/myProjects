package zhenghaozhao.helloworld;

/**
 * Created by Zhenghao on 8/05/2017.
 */

public class test {
    public static void main(String[] args) {
        String e1 = "2*(33-1)";
        Tokenizer t = new SimpleTokenizer(e1);
        Exp exp1 = Exp.parseExp(t);
        Subs subs = new Subs();
        System.out.println(e1 + " = " + exp1.evaluate(subs));
    }

}
