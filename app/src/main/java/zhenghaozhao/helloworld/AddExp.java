package zhenghaozhao.helloworld;

/**
 * Created by Zhenghao on 8/05/2017.
 */

public class AddExp extends Exp {
    Exp left, right;
    AddExp(Exp l, Exp r){
        left = l;
        right = r;
    }
    @Override
    String show(){
        return "("+left.show() + "+" + right.show()+")";
    }
    @Override
    String showSimp() {
        Exp simpLeft = left.simplify();
        Exp simpRight = right.simplify();
        if (simpLeft instanceof LitExp && simpRight instanceof LitExp){
            return ((LitExp) simpLeft).value + ((LitExp) simpRight).value + "";
        }
        return "("+simpLeft.show() + "+" + simpRight.show()+")";
    }


    @Override
    float evaluate(Subs subs) {
        return left.evaluate(subs) + right.evaluate(subs);
    }

    @Override
    int size() {
        return 1 + left.size() + right.size();
    }

    @Override
    int height() {
        return Math.max(1 + left.height(), 1 + right.height());
    }

    @Override
    int operators() {
        return 1 + left.operators() + right.operators();
    }

    @Override
    Exp simplify() {
        if (left instanceof LitExp && right instanceof LitExp){
            return new LitExp(((LitExp) left).value + ((LitExp) right).value);
        }
        else{
            return new AddExp(left.simplify(), right.simplify());
        }
    }
}
