package zhenghaozhao.helloworld;

/**
 * Created by Zhenghao on 8/05/2017.
 */

public class LitExp extends Exp {
    float value;
    LitExp(float value){
        this.value = value;
    }
    @Override
    String showSimp() {
        return value + "";
    }

    @Override
    String show() {
        return ""+value;
    }

    @Override
    float evaluate(Subs subs) {
        return value;
    }

    @Override
    int size() {
        return 1;
    }

    @Override
    int height() {
        return 1;
    }

    @Override
    int operators() {
        return 0;
    }

    @Override
    Exp simplify() {
        return this;
    }


}
