package zhenghaozhao.helloworld;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;

/* prototype code of a simple calculator with four operators (* / + -)*/

public class CalActivity extends AppCompatActivity {
    int numExpressions;
    int count = 0;
    ArrayList<String> pastExpressions;
    String textContent = "";
    static double result;
    Subs subs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cal);
        pastExpressions = new ArrayList<>();
        subs = new Subs();
    }

    public void Equal(View view) {
        Tokenizer t = new SimpleTokenizer(textContent);
        Exp exp = Exp.parseExp(t);
        result = exp.evaluate(subs);
        pastExpressions.add(textContent + " = " + result);
        numExpressions++;
        TextView textView = (TextView)findViewById(R.id.textView3);
        textView.setText(result+"");
    }

    public void errorMessage(String message){
        clearAll();
        Context context = getApplicationContext();
        CharSequence text = message;
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();

    }

    public void clearAll(){
        result = 0;
        textContent = "";
        count = 0;
        TextView textView = (TextView)findViewById(R.id.textView3);
        textView.setText(textContent);

    }

    public void clearText(View view) {
        clearAll();

    }

    public void Replay(View view) {
        count++;
        if (count > numExpressions){
            errorMessage("End of history!");
        }else {
            TextView textView = (TextView) findViewById(R.id.textView3);
            textView.setText(pastExpressions.get(numExpressions - count));
        }
    }


    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button:
                textContent += "1";
                TextView textView1 = (TextView) findViewById(R.id.textView3);
                textView1.setText(textContent);
                break;
            case R.id.button2:
                textContent += "2";
                TextView textView2 = (TextView) findViewById(R.id.textView3);
                textView2.setText(textContent);
                break;
            case R.id.button3:
                textContent += "3";
                TextView textView3 = (TextView) findViewById(R.id.textView3);
                textView3.setText(textContent);
                break;
            case R.id.button4:
                textContent += "4";
                TextView textView4 = (TextView) findViewById(R.id.textView3);
                textView4.setText(textContent);
                break;
            case R.id.button5:
                textContent += "5";
                TextView textView5 = (TextView) findViewById(R.id.textView3);
                textView5.setText(textContent);
                break;
            case R.id.button6:
                textContent += "6";
                TextView textView6 = (TextView) findViewById(R.id.textView3);
                textView6.setText(textContent);
                break;
            case R.id.button7:
                textContent += "7";
                TextView textView7 = (TextView) findViewById(R.id.textView3);
                textView7.setText(textContent);
                break;
            case R.id.button8:
                textContent += "8";
                TextView textView8 = (TextView) findViewById(R.id.textView3);
                textView8.setText(textContent);
                break;
            case R.id.button9:
                textContent += "9";
                TextView textView9 = (TextView) findViewById(R.id.textView3);
                textView9.setText(textContent);
                break;
            case R.id.button11:
                textContent += "0";
                TextView textView10 = (TextView) findViewById(R.id.textView3);
                textView10.setText(textContent);
                break;
            case R.id.button18:
                textContent += ".";
                TextView textView11 = (TextView) findViewById(R.id.textView3);
                textView11.setText(textContent);
                break;
            case R.id.button10:
                textContent += "+";
                TextView textView12 = (TextView) findViewById(R.id.textView3);
                textView12.setText(textContent);
                break;
            case R.id.button12:
                textContent += "-";
                TextView textView13 = (TextView) findViewById(R.id.textView3);
                textView13.setText(textContent);
                break;
            case R.id.button13:
                textContent += "*";
                TextView textView14 = (TextView) findViewById(R.id.textView3);
                textView14.setText(textContent);
                break;
            case R.id.button15:
                textContent += "/";
                TextView textView15 = (TextView) findViewById(R.id.textView3);
                textView15.setText(textContent);
                break;
            case R.id.button23:
                textContent += "(";
                TextView textView16 = (TextView) findViewById(R.id.textView3);
                textView16.setText(textContent);
                break;
            case R.id.button25:
                textContent += ")";
                TextView textView17 = (TextView) findViewById(R.id.textView3);
                textView17.setText(textContent);
                break;
            case R.id.button26:
                textContent += "Sin";
                TextView textView18 = (TextView) findViewById(R.id.textView3);
                textView18.setText(textContent);
                break;

            default:
                errorMessage("Enter an expression");

        }
    }
}
