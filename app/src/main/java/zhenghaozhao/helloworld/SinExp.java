package zhenghaozhao.helloworld;

/**
 * Created by Zhenghao on 12/05/2017.
 */

public class SinExp extends Exp {
    Exp exp;

    SinExp(Exp e){
        exp = e;
    }
    @Override
    String show() {
        return "Sin(" + exp.show() + ")" ;
    }

    @Override
    float evaluate(Subs subs) {
        return (float)Math.sin(exp.evaluate(subs)/((double)180/Math.PI));
    }

    @Override
    int size() {
        return 1 + exp.size();
    }

    @Override
    int height() {
        return 1 + exp.height();
    }

    @Override
    int operators() {
        return 1 + exp.operators();
    }

    @Override
    Exp simplify() {
        return new SinExp(exp.simplify());
    }

    @Override
    String showSimp() {
        if (exp instanceof LitExp){
            return (float)Math.sin(((LitExp) exp).value/((double)180/Math.PI)) + "";
        }
        return "Sin(" + exp.show() + ")";
    }
}
