package zhenghaozhao.helloworld;

/**
 * Created by Zhenghao on 8/05/2017.
 */

public abstract class Tokenizer {
    abstract boolean hasNext();

    abstract Object current();

    abstract void next();

    public void parse(Object o){
        if (current() == null || !current().equals(o))
            throw new Error(); // normally one would use exceptions in such cases, however,
        // it just makes code simpler as you don't need to deal with the exception
        // in calling code
        next();
    }
    public boolean currentis(Object v){
        return (current() != null && current().equals(v));
    }
}
