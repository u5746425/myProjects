package zhenghaozhao.helloworld;

/**
 * Created by Zhenghao on 8/05/2017.
 */

public class VarExp extends Exp {
    String name;
    VarExp(String name) {
        this.name = name;
    }
    @Override
    String show() {
        return null;
    }

    @Override
    float evaluate(Subs subs) {
        return 0;
    }

    @Override
    int size() {
        return 0;
    }

    @Override
    int height() {
        return 0;
    }

    @Override
    int operators() {
        return 0;
    }

    @Override
    Exp simplify() {
        return null;
    }

    @Override
    String showSimp() {
        return null;
    }
}
