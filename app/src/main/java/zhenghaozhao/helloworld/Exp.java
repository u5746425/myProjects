package zhenghaozhao.helloworld;

/**
 * Created by Zhenghao on 8/05/2017.
 */

public abstract class Exp {
    abstract String show();
    abstract float evaluate(Subs subs);
    abstract int size();
    abstract int height();
    abstract int operators();
    abstract Exp simplify();
    abstract String showSimp();
    /*
    Grammar:
    e.g. exp = (1 + 2) * 2
     */


    static public Exp parseExp(Tokenizer tok) {
        Exp term = parseTerm(tok);
        if (tok.currentis("+")){
            tok.next();
            Exp exp = parseExp(tok);
            return new AddExp(term,exp);
        }else if (tok.currentis("-")){
            tok.next();
            Exp exp = parseExp(tok);
            return new MinuExp(term,exp);
        }
        else {return term;}
    }

    private static Exp parseTerm(Tokenizer tok){
        Exp fact = parseFactor(tok);
        if (tok.currentis("*")){
            tok.next();
            Exp term = parseTerm(tok);
            return new MultExp(fact,term);
        }else if (tok.currentis("/")){
            tok.next();
            Exp term = parseTerm(tok);
            return new DivExp(fact,term);
        }
        else {return fact;}
    }

    private static Exp parseFactor(Tokenizer tok){
        if (tok.currentis("Sin")){
            tok.next();
            Exp exp = parseExp(tok);
            tok.next();
            return new SinExp(exp);
        }
        else if (tok.current() instanceof Float){
            Exp exp = new LitExp((Float) tok.current());
            tok.next();
            return exp;
        }else if (tok.currentis("(")){
            tok.next();
            Exp exp = parseExp(tok);
            tok.parse(")");
            return exp;
        }
        else{
            Exp exp = new VarExp((String) tok.current());
            tok.next();
            return exp;
        }
    }

}
